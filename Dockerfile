FROM node:8-alpine

# Create app directory
WORKDIR /app

COPY . /app

# Install app dependencies
RUN yarn install

RUN yarn build

EXPOSE 80

CMD [ "yarn", "serve" ]

