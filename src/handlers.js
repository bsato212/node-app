import axios from 'axios';
import get from 'lodash/get';
import LRU from 'lru-cache';

let incValue = 0;
const githubBaseUrl = 'https://api.github.com/users';

const cache = LRU( {
	max: 100,
	maxAge: 1000 * 30,
} );

export function helloHandler( req, res ) {
	res.send( `[${Date.now()}] Node.js version ${process.versions.node}` );
}

export function incHandler( req, res ) {
	res.send( `[${Date.now()}] ${incValue}` );
	incValue++;
}

export function githubHandler( req, res ) {
	const id = get( req, 'params.id', 'bsato212' );
	const githubUrl = `${githubBaseUrl}/${id}`;

	const cachedResponse = cache.get( githubUrl );
	if ( cachedResponse ) {
		res.send( `[${Date.now()}][from cache] ${cachedResponse}` );
		return;
	}

	axios.get( githubUrl, {
		responseType: 'json',
		timeout: 2000,
	} )
		.then( data => {
			const message = `${get( data, 'data.login' )} ${get( data, 'data.name' )}`;
			res.send( `[${Date.now()}] ${message}` );
			cache.set( githubUrl, message );
		} )
		.catch( error => {
			res.send( `[${Date.now()}] Error: ${error.message}` );
		} );
}
