import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import morgan from 'morgan';

import { helloHandler, incHandler, githubHandler } from './handlers';

const PORT = process.env.PORT || 80;
const app = express();

app.use( helmet() );
app.use( compression() );
app.use( morgan( 'short' ) );

app.get( '/', helloHandler );
app.get( '/inc', incHandler );
app.get( '/github', githubHandler );
app.get( '/github/:id', githubHandler );

app.listen( PORT );
